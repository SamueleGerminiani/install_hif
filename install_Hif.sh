#!/bin/bash

NThreads="`grep -c ^processor /proc/cpuinfo`"

if [ $# -eq 0 ]
then
    installPrefix="/usr/local"
else
    installPrefix="$1"
fi

if [ ! -d "hif" ]; then
    mkdir hif
    mkdir hif/a2tool  hif/build_utils  hif/core  hif/ddt  hif/hdtlib  hif/third_party
    mkdir hif/a2tool/build  hif/core/build  hif/ddt/build hif/hdtlib/build  hif/third_party/build
fi

cd hif 

if [ ! -d "a2tool/repo" ]; then
    cd a2tool 
    git clone --single-branch --branch dev https://gitlab.com/edalab/hifsuite/mainline/a2tool.git repo
    cd ..
fi

if [ ! -d "ddt/repo" ]; then
    cd ddt 
    git clone --single-branch --branch dev https://gitlab.com/edalab/hifsuite/mainline/ddt.git repo
    cd ..
fi

if [ ! -d "core/repo" ]; then
    cd core 
    git clone --single-branch --branch dev https://gitlab.com/edalab/hifsuite/mainline/core.git repo
    cd ..
fi

if [ ! -d "hdtlib/repo" ]; then
    cd hdtlib 
    git clone --single-branch --branch dev https://gitlab.com/edalab/hifsuite/mainline/hdtlib.git repo
    cd ..
fi

if [ ! -d "build_utils/repo" ]; then
    cd build_utils 
    git clone --single-branch --branch dev https://gitlab.com/edalab/hifsuite/mainline/build_utils.git repo
    cd ..
fi

if [ ! -d "third_party/repo" ]; then
    cd third_party 
    git clone https://gitlab.com/edalab/hifsuite/mainline/third_party.git repo
    cd ..
fi


sudo apt-get install flex bison

if ! /usr/bin/gcc-8 -v COMMAND &> /dev/null
then
    sudo add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    sudo apt-get install gcc-8 g++-8
else
    echo "found gcc-8"
fi



echo 'export CXX=/usr/bin/g++-8'> set-vars.sh
echo 'export CC=/usr/bin/gcc-8'>> set-vars.sh
expLP=export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"${installPrefix}"/lib
echo "${expLP}">> set-vars.sh
. set-vars.sh
rm set-vars.sh

poco="$installPrefix"/lib/libPocoFoundation.so
if test -f "$poco"; then
    echo "Poco xml found!"
else
    wget https://pocoproject.org/releases/poco-1.8.1/poco-1.8.1.tar.gz

    sudo apt-get install openssl libssl-dev
    sudo apt-get install libiodbc2 libiodbc2-dev
    sudo apt-get install libmysqlclient-dev

    tar -xvf poco-1.8.1.tar.gz
    cd poco-1.8.1
    cd build
    cmake -DCMAKE_INSTALL_PREFIX="$installPrefix" ..
    sudo make -j"$NThreads"
    sudo make install
    cd ../../
    rm -rf poco-1.8.1
    rm -f poco-1.8.1.tar.gz
fi

systemc="$installPrefix"/systemc-2.3.2/lib-linux64/libsystemc-2.3.2.so
if test -f "$systemc"; then
    echo "System C found!"
else
    sudo apt-get install build-essential
    wget https://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.2.tar.gz
    tar -xvf systemc-2.3.2.tar.gz && rm -f systemc-2.3.2.tar.gz
    cd systemc-2.3.2/ && mkdir build && cd build
    sudo ../configure --prefix="$installPrefix"/systemc-2.3.2/
    sudo make -j"$NThreads"
    sudo make install
    cd ../../
    sudo rm -rf systemc-2.3.2
    echo '/usr/local/systemc-2.3.2/'> set-vars.sh
    expSCH=export SYSTEMC_HOME="${installPrefix}"/systemc-2.3.2/
    echo "${expSCH}">> set-vars.sh
    expSCLP=export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"${installPrefix}"/systemc-2.3.2/lib-linux64
    echo "${expSCLP}">> set-vars.sh
    . set-vars.sh
    rm set-vars.sh
fi


cd hdtlib/build
cmake ../repo -DCMAKE_CXX_STANDARD=17 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="$installPrefix" -DEdalabSystemC_HINT_AMS_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_AMS_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_RTL_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_RTL_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_TLM_INCLUDE="$installPrefix"/systemc-2.3.2/include
make -j"$NThreads"
sudo make install
cd ../../

if test ! -f "${installPrefix}/lib/libhif.so"; then
echo -e "add_definitions(-w)\n$(cat core/repo/CMakeLists.txt)" > core/repo/CMakeLists.txt
fi
cd core/build
cmake ../repo -DCMAKE_CXX_STANDARD=17 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="$installPrefix" -DEdalabSystemC_HINT_AMS_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_AMS_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_RTL_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_RTL_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_TLM_INCLUDE="$installPrefix"/systemc-2.3.2/include
make -j"$NThreads"
sudo make install
cd ../../

cd a2tool/build
cmake ../repo -DCMAKE_CXX_STANDARD=17 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="$installPrefix" -DEdalabSystemC_HINT_AMS_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_AMS_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_RTL_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_RTL_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_TLM_INCLUDE="$installPrefix"/systemc-2.3.2/include
make -j"$NThreads"
sudo make install
cd ../../

cd ddt/build
cmake ../repo -DCMAKE_CXX_STANDARD=17 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="$installPrefix" -DEdalabSystemC_HINT_AMS_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_AMS_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_RTL_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_RTL_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_TLM_INCLUDE="$installPrefix"/systemc-2.3.2/include
make -j"$NThreads"
sudo make install
cd ../../

cd third_party/build
cmake ../repo -DCMAKE_CXX_STANDARD=17 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="$installPrefix" -DEdalabSystemC_HINT_AMS_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_AMS_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_RTL_INCLUDE="$installPrefix"/systemc-2.3.2/include -DEdalabSystemC_HINT_RTL_LIB="$installPrefix"/systemc-2.3.2/lib-linux64 -DEdalabSystemC_HINT_TLM_INCLUDE="$installPrefix"/systemc-2.3.2/include
make -j"$NThreads"
sudo make install
cd ../../../

toAdd="Add\nexport "LD_LIBRARY_PATH='$LD_LIBRARY_PATH':"${installPrefix}""lib\nto your bashrc"
echo -e "${toAdd}"
